return {
  "danymat/neogen",
  opts = {
    snippet_engine = "luasnip",
    enable = true,
    languages = {
      lua = {
        template = {
          annotation_convention = "ldoc",
        },
      },
      python = {
        template = {
          annotation_convention = "google_docstrings",
        },
      },
      rust = {
        template = {
          annotation_convention = "rustdoc",
        },
      },
      javascript = {
        template = {
          annotation_convention = "jsdoc",
        },
      },
      typescript = {
        template = {
          annotation_convention = "tsdoc",
        },
      },
      go = {
        template = {
          annotation_convention = "godoc",
        },
      },
    },
  },
  keys = {
    {
      "<leader>cdg",
      function()
        require("neogen").generate()
      end,
      desc = "Annotation",
    },
    {
      "<leader>cdc",
      function()
        require("neogen").generate { type = "class" }
      end,
      desc = "Class",
    },
    {
      "<leader>cdf",
      function()
        require("neogen").generate { type = "func" }
      end,
      desc = "Function",
    },
    {
      "<leader>cdt",
      function()
        require("neogen").generate { type = "type" }
      end,
      desc = "Type",
    },
  },
}
