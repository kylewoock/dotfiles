return {
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, { "javascript", "typescript", "tsx", "graphql" })
    end,
  },
  {
    "williamboman/mason.nvim",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, { "typescript-language-server", "js-debug-adapter", "graphql-language-service-cli" })
    end,
  },
  {
    "pmizio/typescript-tools.nvim",
    dependencies = { "folke/neoconf.nvim", cmd = "Neoconf", config = true },
    opts = {
      tsserver_file_preferences = {
        includeInlayParameterNameHints = "all",
        includeInlayParameterNameHintsWhenArgumentMatchesName = true,
        includeInlayFunctionParameterTypeHints = true,
        includeInlayVariableTypeHints = true,
        includeInlayVariableTypeHintsWhenTypeMatchesName = true,
        includeInlayPropertyDeclarationTypeHints = true,
        includeInlayFunctionLikeReturnTypeHints = true,
        includeInlayEnumMemeberValueHints = true,
      },
    },
    config = function(_, opts)
      require("plugins.lsp.utils").on_attach(function(client, bufnr)
        if client.name == "tsserver" then
          vim.keymap.set("n", "<leader>lo", "<cmd>TSToolsOrganizeImports<CR>", { buffer = bufnr, desc = "Organize Imports" })
          vim.keymap.set("n", "<leader>lO", "<cmd>TSToolsSortImports<CR>", { buffer = bufnr, desc = "Sort Imports" })
          vim.keymap.set("n", "<leader>lu", "<cmd>TSToolsRemoveUnused<CR>", { buffer = bufnr, desc = "Remove Unused" })
          vim.keymap.set("n", "<leader>lz", "<cmd>TSToolsGoToSourceDefinition<CR>", { buffer = bufnr, desc = "Go To Source Definition" })
          vim.keymap.set("n", "<leader>lR", "<cmd>TSToolsRemoveUnusedImports<CR>", { buffer = bufnr, desc = "Remove Unused Imports" })
          vim.keymap.set("n", "<leader>lF", "<cmd>TSToolsFixAll<CR>", { buffer = bufnr, desc = "Fix All" })
          vim.keymap.set("n", "<leader>lA", "<cmd>TSToolsAddMissingImports<CR>", { buffer = bufnr, desc = "Add Missing Imports" })
        end
      end)
      require("typescript-tools").setup(opts)
    end,
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = { "pmizio/typescript-tools.nvim" },
    opts = {
      servers = {
        -- ESLINT
        eslint = {
          settings = {
            workingDirectory = { mode = "auto" },
          },
        },
      },
      setup = {
        eslint = function()
          vim.api.nvim_create_autocmd("BufWritePre", {
            callback = function(event)
              local client = vim.lsp.get_active_clients({ bufnr = event.buf, name = "eslint" })[1]
              if client then
                local diag = vim.diagnostic.get(event.buf, { namespace = vim.lsp.diagnostic.get_namespace(client.id) })
                if #diag > 0 then
                  vim.cmd "EslintFixAll"
                end
              end
            end,
          })
        end,
      },
    },
  },
  -- {
  --   "mfussenegger/nvim-dap",
  --   opts = {
  --     setup = {
  --       vscode_js_debug = function()
  --         local function get_js_debug()
  --           local install_path = require("mason-registry").get_package("js-debug-adapter"):get_install_path()
  --           return install_path .. "/js-debug/src/dapDebugServer.js"
  --         end

  --         for _, adapter in ipairs { "pwa-node", "pwa-chrome", "pwa-msedge", "node-terminal", "pwa-extensionHost" } do
  --           require("dap").adapters[adapter] = {
  --             type = "server",
  --             host = "localhost",
  --             port = "${port}",
  --             executable = {
  --               command = "node",
  --               args = {
  --                 get_js_debug(),
  --                 "${port}",
  --               },
  --             },
  --           }
  --         end

  --         for _, language in ipairs { "typescript", "javascript" } do
  --           require("dap").configurations[language] = {
  --             {
  --               type = "pwa-node",
  --               request = "launch",
  --               name = "Launch file",
  --               program = "${port}",
  --               cwd = "${workspaceFolder}",
  --             },
  --             {
  --               type = "pwa-node",
  --               request = "attach",
  --               name = "Attach",
  --               processId = require("dap.utils").pick_process,
  --               cwd = "${workspaceFolder}",
  --             },
  --             {
  --               type = "pwa-node",
  --               request = "launch",
  --               name = "Debug Jest Tests",
  --               runtimeExecutable = "node",
  --               runtimeArgs = {
  --                 "./node_modules/jest/bin/jest.js",
  --                 "--runInBand",
  --               },
  --               rootPath = "${workspaceFolder}",
  --               cwd = "${workspaceFolder}",
  --               console = "integratedTerminal",
  --               internalConsoleOptions = "neverOpen",
  --             },
  --             {
  --               type = "pws-chrome",
  --               name = "Attach - Remote Debugging",
  --               request = "attach",
  --               program = "${file}",
  --               cwd = vim.fn.getcwd(),
  --               sourceMaps = true,
  --               protocol = "inspector",
  --               port = 9222,
  --               webRoot = "${workspaceFolder}",
  --             },
  --           }
  --         end

  --         for _, language in pairs { "typescriptreact", "javscriptreact" } do
  --           require("dap").configurations[language] = {
  --             {
  --               type = "pwa-chrome",
  --               name = "Attach - Remote Debugging",
  --               request = "attach",
  --               cwd = vim.fn.getcwd(),
  --               sourceMaps = true,
  --               protocol = "inspector",
  --               port = 9222,
  --               webRoot = "${workspaceFolder}",
  --             },
  --             {
  --               type = "pwa-chrome",
  --               name = "Launch Chrome",
  --               request = "launch",
  --               url = "http://localhost:5173",
  --               webRoot = "${workspaceFolder}",
  --               useDataDire = "${workspaceFolder}/.vscode/vscode-chrome-debug-userdatadir",
  --             },
  --           }
  --         end
  --       end,
  --     },
  --   },
  -- },
  -- {
  --   "nvim-neotest/neotest",
  --   dependencies = {
  --     "nvim-neotest/neotest-jest",
  --     "marilari88/neotest-vitest",
  --     "thenbe/neotest-playwright",
  --   },
  --   opts = function(_, opts)
  --     vim.list_extend(opts.apdaters {
  --       require "neotest-jest",
  --       require "neotest-vitest",
  --       require("neotest-playwrite").adapter {
  --         options = {
  --           persist_project_selection = true,
  --           enable_dynamic_test_discovery = true,
  --         },
  --       },
  --     })
  --   end,
  -- },
}
