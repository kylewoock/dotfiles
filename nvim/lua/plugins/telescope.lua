local function find_files()
  local opts = {}
  local telescope = require "telescope.builtin"

  local ok = pcall(telescope.git_files, opts)
  if not ok then
    telescope.find_files(opts)
  end
end

return {
  "nvim-telescope/telescope.nvim",
  dependencies = {
    "nvim-lua/plenary.nvim",
    { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
    "nvim-tree/nvim-web-devicons",
    "ahmedkhalf/project.nvim",
    "nvim-telescope/telescope-project.nvim",
    "cljoly/telescope-repo.nvim",
  },
  cmd = "Telescope",
  keys = {
    { "<leader><space>", find_files, desc = "Find Files" },
    { "<leader>ff", find_files, desc = "Find Files" },
    { "<leader>fo", "<cmd>Telescope oldfiles<cr>", desc = "Recent" },
    { "<leader>fg", "<cmd>Telescope git_files<cr>", desc = "Git Files" },
    { "<leader>f/", "<cmd>Telescope live_grep<cr>", desc = "Grep" },
    { "<leader>hs", "<cmd>Telescope help_tags<cr>", desc = "Help Tags" },
    {
      "<leader>pp",
      function()
        require("telescope").extensions.project.project { display_type = "minimal" }
      end,
      desc = "List Project",
    },
    { "<leader>ps", "<cmd>Telescope repo list<cr>", desc = "Project Repo List" },
    { "<leader>sw", "<cmd> Telescope live_grep<CR>", desc = "Workspace" },
    {
      "<leader>sb",
      function()
        require("telescope.builtin").current_buffer_fuzzy_find()
      end,
      desc = "Search Buffer",
    },
    { "<leader>bb", "<cmd>Telescope buffers<cr>", desc = "Buffers" },
  },
  config = function()
    local telescope = require "telescope"
    -- local icons = require "config.icons"
    local actions = require "telescope.actions"
    local actions_layout = require "telescope.actions.layout"

    local mappings = {
      i = {
        ["<C-j>"] = actions.move_selection_next,
        ["<C-k>"] = actions.move_selection_previous,
        ["<C-n>"] = actions.cycle_history_next,
        ["<C-p>"] = actions.cycle_history_prev,
        ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
        ["?"] = actions_layout.toggle_preview,
      }
    }

    local opts = {
      defaults = {
        mappings = mappings,
        border = {},
        borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
        color_devicons = true,
      },
      exntensions = {
        project = {
          base_dirs = {
            "~/code",
          },
          hidden_files = true,
          theme = "dropdown"
        },
        repo = {
          list = {
            search_dirs = {
              "~/code",
            }
          }
        }
      },
    }

    telescope.setup(opts)
    telescope.load_extension "fzf"
    telescope.load_extension "project"
    telescope.load_extension "repo"
  end
}
