return {
  "windwp/nvim-autopairs",
  event = "InsertEnter",
  enabled = true,
  config = function()
    local npairs = require "nvim-autopairs"
    npairs.setup {
      check_ts = true,
    }
  end,
}
