return {
  "lukas-reineke/indent-blankline.nvim",
  main = "ibl",
  event = { "BufReadPre", "BufNewFile" },
  -- tags = "3.5.4",
  opts = {
    indent = {
      char = "┊",
      tab_char = ">",
    },
    whitespace = {
      remove_blankline_trail = false,
    },
    scope = { 
      enabled = true,
    },
    exclude = {
      filetypes = {
        "help",
        "alpha",
        "dasbhoard",
        "neo-tree",
        "Trouble",
        "mason",
        "lazy",
        "notify",
        "toggleterm",
        "lazyterm",
      },
    },
  },
}
