return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 500
  end,
  config = function()
    local wk = require "which-key"
    wk.setup {
      show_help = false,
      plugins = { spelling = true},
    }
    wk.add({
      { "<leader>f", group = "+File" },
      { "<leader>b", group = "+Buffer" },
      { "<leader>g", group = "+Git" },
      { "<leader>l", group = "+Code Diag" },
      { "<leader>c", group = "+Code" },
      { "<leader>h", group = "+Help" },
      { "<leader>p", group = "+Project" },
      { "<leader>s", group = "+Search"},
      { "<leader>w", "<cmd>update!<CR>", desc = "Save"}
    })
  end,
}
