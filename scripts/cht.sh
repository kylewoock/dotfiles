#!/usr/bin/env zsh
languages=`echo "golang rust lua python typescript nodejs" | tr ' ' '\n'`
utils=`echo "xargs find sed awk grep" | tr ' ' '\n'`

selected=`printf "$languages\n$utils" | fzf`
read "query?query: "

if printf $languages | grep -qs "$selected"; then
    curl cht.sh/$selected/`echo $query | tr ' ' '+'`
else
    curl cht.sh/$selected~$query
fi
