#!/usr/bin/env zsh
accounts=`echo "kw-sbx shared-svc-dev shared-svc-prod infra-devops" | tr ' ' '\n'`
selected=`printf $accounts | fzf`
if [[ -n $AWS_VAULT ]]; then
    unset AWS_VAULT
fi

aws-vault login $selected -t $(bw get totp aws-cli)
