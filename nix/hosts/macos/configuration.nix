{ config, pkgs, lib, ...}:
{
  imports = [
    ./../../config/base.nix
    ./../../config/darwin.nix
    ./../../config/homebrew.nix
  ];

  nixpkgs.hostPlatform = "aarch64-darwin";

  users.users.kylewoock = {
    name = "kylewoock";
    home = "/Users/kylewoock";
  };

  environment.darwinConfig = "$HOME/.nixpkgs/hosts/work/configuration.nix";
}
