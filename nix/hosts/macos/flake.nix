{
  description = "Work macbook setup";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nix-darwin.url = "github:LnL7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.url = "github:nix-community/home-manager";
  };

  outputs = inputs@{ self, nix-darwin, nixpkgs, home-manager }: {
    # Put hostname of work computer here; scutil --get LocalHostName
    darwinConfigurations."Kyles-MacBook-Pro-2" = nix-darwin.lib.darwinSystem {
      modules = [
        ./configuration.nix
        home-manager.darwinModules.home-manager
      ];
    };
    darwinConfigurations."test" = nix-darwin.lib.darwinSystem {
      modules = [
        ./configuration.nix
        ./test.nix
        home-manager.darwinModules.home-manager
      ];
    };
  };
}
