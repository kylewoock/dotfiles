{ config, pkgs, ... }:
{

  services.nix-daemon.enable = true;
  nix.package = pkgs.nix;
  nix.settings.experimental-features = ["nix-command" "flakes"];

  # Create /etc/zshrc that loads the nix-darwin environment.
  programs.zsh.enable = true;

  system.stateVersion = 4;

  environment.systemPackages = with pkgs; [
    home-manager
    nixd
    cloc
    git
    gitAndTools.git-absorb
    git-crypt
    pre-commit
    awscli2
    aws-vault

    nodejs-slim_18
    nodePackages.npm
    nodePackages.yarn
    nodePackages.pnpm

    pipenv
    python3
    python3Packages.pip
    poetry
    pdm

    go

    postgresql_13
    redis

    fd
    htop
    ripgrep
    tmux
    neovim
    wezterm
    delta
    tree
    openssl

    zsh
    zsh-autosuggestions
    zsh-nix-shell
    zsh-syntax-highlighting
    tmux

    jq
    yq
    iperf
  ];

  fonts = {
    packages = with pkgs; [
      material-design-icons
      font-awesome
      (nerdfonts.override {
        fonts = [
          "FiraCode"
          "JetBrainsMono"
        ];
      })
    ];
  };
}
