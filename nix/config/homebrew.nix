{config, pkgs, lib, ...}:
{
  homebrew = {
    casks = [
      "firefox"
      "brave"

      "docker"
      "insomnia"

      "drawio"
      "obsidian"
    ];
  };
}
