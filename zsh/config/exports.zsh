export LANG=en_US.UTF-8

export EDITOR="nvim"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_BIN_HOME="$HOME/.local/bin"

export STARSHIP_CONFIG="$XDG_CONFIG_HOME/starship/starship.toml"
# export DOOMBIN="$HOME/.emacs.d/bin"

PATH="$PATH:$XDG_BIN_HOME"
# PATH="$PATH:$DOOMBIN"
PATH="/opt/homebrew/bin:$PATH"
PATH="$HOME/.asdf/installs/golang/1.21.4/packages/bin/:$PATH"
PATH="/Library/TeX/texbin/:$PATH"

# TODO: This can't be done like this because asdf hasn't been loaded yet
# export FZF_BASE="$(asdf where fzf)"
export FZF_BASE="/Users/kylewoock/.asdf/installs/fzf/0.43.0"
