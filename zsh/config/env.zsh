ZSH_TMUX_AUTOSTART=true
ZSH_TMUX_AUTOCONNECT=true
ZSH_TMUX_AUTOQUIT=true

export FZF_DEFAULT_OPTS="--height 50% --ansi"
export FZF_DEFAULT_COMMAND="rg --files --no-ignore --hidden --follow --glob '!.git/*'"
# export FZF_BASE="/Users/woockk/.asdf/installs/fzf/0.34.0"



