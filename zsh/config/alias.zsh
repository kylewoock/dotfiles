[ -x "$(command -v nvim)" ] && alias vim="nvim"
alias vimdiff="nvim -d"

alias v="$EDITOR"
alias sv="sudo -E $EDITOR"

alias md="mkdir -pv"
alias cp="cp -iv"
alias mv="mv -iv"
alias rm="rm -iv"

alias -g l="eza -la --icons --no-permissions --no-filesize --no-user --no-time"
alias -g ll="eza -lh --icons"
alias -g ls="eza --git --icons"
alias -g lt="eza -lT --git --icons"

alias cat="bat --paging=never"
alias catp="bat -p --paging=never"

alias cd="z"

alias cl="clear"
alias df="df -h"
alias ka="killall"

alias g="git"
alias ga="git add"
alias gc="git commit"
alias gb="git branch"
alias gcb="git checkout -b"
alias gc!="git checkout -v --amend"
alias gcm="git commit -m"
alias gd="git diff"
alias gcl="git clone"
alias gp="git push"
alias gP="git pull"
alias gst="git status"

alias d="$HOME/.config"
alias D="$HOME/Downloads"
alias c="$HOME/code"
# TODO: Change this to obsidian vault
alias n="$HOME/notes"

