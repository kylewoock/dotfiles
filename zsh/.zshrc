# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export ZDOTDIR="$HOME/.config/zsh"
export ZSH_CONFIG="$ZDOTDIR/config"
export ZSH_CACHE="$HOME/.cache/zsh"
export ZSH="$ZSH_CONFIG/oh-my-zsh"

# Set theme, this gets overridden by starship for now
ZSH_THEME="powerlevel10k/powerlevel10k"

# Auto update behavior
# zstyle ':omz:update' mode disabled
# zstyle ':omz:update' mode auto
zstyle ':omz:update' mode reminder

# Frequency in days how often to check for updates
zstyle ':omz:update' frequency 13

export FZF_BASE="/Users/kylewoock/.asdf/installs/fzf/0.32.1"
# Run Exports here instead of below so FZF_BASE can be exported
source $ZSH_CONFIG/exports.zsh
source $ZSH_CONFIG/env.zsh
source $ZSH_CONFIG/ikon_exports.zsh

# Plugins to enable
# ASDF needs to come first so it is loaded before the others
plugins=(
    asdf
    fzf
    git
    zsh-syntax-highlighting
    zsh-autosuggestions)

eval "$(zoxide init zsh)"

source $ZSH/oh-my-zsh.sh
source $ZSH_CONFIG/alias.zsh


# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh

# To customize prompt, run `p10k configure` or edit ~/.dotfiles/zsh/.p10k.zsh.
[[ ! -f ~/.dotfiles/zsh/.p10k.zsh ]] || source ~/.dotfiles/zsh/.p10k.zsh
