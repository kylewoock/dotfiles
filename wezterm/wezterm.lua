local wezterm = require("wezterm")

local config = {}

-- config.font = wezterm.font("JetBrainsMono Nerd Font")
config.font = wezterm.font("SauceCodePro Nerd Font")
config.font_size = 22

-- config.color_scheme = "Gruvbox Dark (Gogh)"
-- config.color_scheme = "Tokyo Night (Gogh)"
config.color_scheme = "Catppuccin Mocha (Gogh)"

config.window_frame = {
  font = wezterm.font{ family = "JetBrainsMono Nerd Font", weight = "Bold"},

  font_size = 14.0,
}

-- config.default_prog = {"/opt/homebrew/bin/tmux"}
-- config.default_prog = {"zsh", "-c", "/opt/homebrew/bin/tmux -f ~/.tmux.conf"}

config.colors = {
  tab_bar = {
    active_tab = {
      fg_color = '#1f2335',
      bg_color = '#a9b1d6',
      intensity = 'Bold',
    },
  }
}

config.enable_tab_bar = false
config.tab_bar_at_bottom = true
config.use_fancy_tab_bar = true
config.window_decorations = "RESIZE"

-- config.leader = {
--   key = "a",
--   mods = "CTRL",
--   timeout_milliseconds = 2000, 
-- }

config.keys = {
  -- {
  --   key = "[",
  --   mods = "LEADER",
  --   action = wezterm.action.ActivateCopyMode,
  -- },
  {
    key = "f",
    mods = "ALT",
    action = wezterm.action.TogglePaneZoomState,
  },
  -- {
  --   key = "c",
  --   mods = "LEADER",
  --   action = wezterm.action.SpawnTab 'CurrentPaneDomain',
  -- },
  -- {
  --   key = "n",
  --   mods = "LEADER",
  --   action = wezterm.action.ActivateTabRelative(1),
  -- },
  -- {
  --   key = "p",
  --   mods = "LEADER",
  --   action = wezterm.action.ActivateTabRelative(-1),
  -- },
  -- {
  --   key = ",",
  --   mods = "LEADER",
  --   action = wezterm.action.PromptInputLine {
  --     description = "Enter new name for tab",
  --     action = wezterm.action_callback(
  --       function(window, pane, line)
  --         if line then
  --           window:active_tab():set_title(line)
  --         end
  --       end
  --     )
  --   }
  -- },
  -- {
  --   key = "w",
  --   mods = "LEADER",
  --   action = wezterm.action.ShowTabNavigator,
  -- },
  -- {
  --   key = "&",
  --   mods = "LEADER|SHIFT",
  --   action = wezterm.action.CloseCurrentTab{ confirm = true },
  -- },
  -- {
  --   key = 'h',
  --   mods = 'LEADER',
  --   action = wezterm.action({ EmitEvent = "move-left" }),
  -- },
  -- {
  --   key = 'j',
  --   mods = 'LEADER',
  --   action = wezterm.action({ EmitEvent = "move-down" }),
  -- },
  -- {
  --   key = 'k',
  --   mods = 'LEADER',
  --   action = wezterm.action({ EmitEvent = "move-up" }),
  -- },
  -- {
  --   key = 'l',
  --   mods = 'LEADER',
  --   action = wezterm.action({ EmitEvent = "move-right" }),
  -- }, 
  -- {
  --   key = 'H',
  --   mods = 'LEADER|SHIFT',
  --   action = wezterm.action({ EmitEvent = "resize-left" }),
  -- },
  -- {
  --   key = 'J',
  --   mods = 'LEADER|SHIFT',
  --   action = wezterm.action({ EmitEvent = "resize-down" }),
  -- },
  -- {
  --   key = 'K',
  --   mods = 'LEADER|SHIFT',
  --   action = wezterm.action({ EmitEvent = "resize-up" }),
  -- },
  -- {
  --   key = 'L',
  --   mods = 'LEADER|SHIFT',
  --   action = wezterm.action({ EmitEvent = "resize-right" }),
  -- },
  -- {
  --   key = '|',
  --   mods = 'LEADER|SHIFT',
  --   action = wezterm.action.SplitPane {
  --     DIRECTION = "RIGHT",
  --     SIZE = { PERCENT = 50 }
  --   }
  -- },
  -- {
  --   KEY = "-",
  --   MODS = "LEADER|SHIFT",
  --   ACTION = WEZTERM.ACTION.SPLITPANE {
  --     DIRECTION = "DOWN",
  --     SIZE = { PERCENT = 50 },
  --   }
  -- }

}

return config
