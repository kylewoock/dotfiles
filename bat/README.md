# BAT Themes

## Add Themes to BAT

- Add .tmTheme file to `.config/bat/themes`
- rebuild bat cache `bat cache --build`
- Set new theme configuration
