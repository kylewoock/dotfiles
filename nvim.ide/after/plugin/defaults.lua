local api = vim.api
local g = vim.g
local opt = vim.opt

-- remap leader and local leader to <SPACE>
-- :h leader
api.nvim_set_keymap("", "<Space>", "<Nop>", { noremap = true, silent = true })
g.mapleader = " "
g.maplocalleader = " "


-- All of these settings can be looked up in :h
opt.termguicolors = true -- Enables Terminal GUI Colors
opt.hlsearch = true -- Set highlight when searching
opt.number = true -- Set Line numbers
opt.relativenumber = true -- set line numbers to be relative
opt.mouse = "a" -- Enables Mouse support (TODO: remove mouse support)
opt.breakindent = true -- enable break indent
opt.undofile = true -- Save undo history
opt.ignorecase = true -- Case insensitive searching unless using /C or Capital search
opt.smartcase = true -- Turn on smartcase
opt.updatetime = 250 -- Decrease Update time
opt.signcolumn = "yes" -- Alway show sign column
opt.clipboard = "unnamedplus" -- Use System clipboard
opt.timeoutlen = 300 -- Time in milliseconds to wait for a mapped sequence to complete
-- opt.laststatus = 3 -- set if you want global status
opt.showmode = fase -- Do not need to show mode. Lualine does this instead
opt.scrolloff = 8 -- Lines of context
-- opt.scrolloff = 999 -- lines of context
-- opt.sidescrolloff = 999 
opt.joinspaces = false
-- TODO: Not sure why this doesn't show up properly
-- opt.sessionoptions = "buffers,curdir,help,tabpages,winsize,winops,terminal"
opt.smartindent = true -- smart indents
opt.expandtab = true
opt.smarttab = true
opt.textwidth = 0
opt.autoindent = true
opt.shiftwidth = 4
opt.tabstop = 4
opt.softtabstop = 4
opt.cmdheight = 0

-- Search Path
opt.path:remove "/usr/include"
opt.path:append "**"
-- vim.cmd [[set path=.,,,$PWD/**]] -- Alternative to set path

opt.wildignorecase = true
opt.wildignore:append "**/.git/*"
opt.wildignore:append "**/build/*"
-- Other common options set --
-- opt.wildmode
-- opt.wildmenu
-- opt.wildchars
-- opt.suffix
-- opt.suffixsadd

-- Better NetRW
-- :h netrw-ref
-- :h netrw-command-settings
g.netrw_banner = 0 -- Hide banner
g.netrw_browse_split = 4 -- Open in previous window
g.netrw_altv = 1 -- Open with right splitting
g.netrw_liststyle = 3 -- Tree-style view
g.netrw_list_hide = (vim.fn["netrw_gitignore#Hide"]()) .. [[,\(^\|\s\s\)\zs\.\S\+]] -- use .gitignore

-- Highlight on Yank
vim.cmd [[
	augroup YankHighlight
		autocmd!
		autocmd TextYankPost * silent! lua vim.highlight.on_yank()
	augroup end

]]
