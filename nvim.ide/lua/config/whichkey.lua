local M = {}

function M.setup()
	-- Might want to do the pcall thing here
	local whichkey = require "which-key"

	local conf = {
		window = {
			border = "single",
			position = "botton",
		},
	}

	local opts = {
		mode = "n",
		prefix = "<leader>",
		buffer = nil,
		silent = true,
		noremap = true,
		nowait = false,
	}

	local mappings = {
		["w"] = { "<cmd>update!<CR>", "Save" },
		["q"] = { "<cmd>q!<CR>", "Quit" },

		b = {
			name = "Buffer",
			c = { "<cmd>bd!<CR>", "Close Current Buffer" },
			D = { "<cmd>%bd|e#|bd#<CR>", "Delete all buffers" },
		},

		z = {
			name = "Packer",
			c = { "<cmd>PackerCompile<CR>", "Compile" },
			i = { "<cmd>PackerInstall<CR>", "Install" },
			s = { "<cmd>PackerSync<CR>", "Sync" },
			S = { "<cmd>PackerStatus<CR>", "Status" },
			u = { "<cmd>PackerUpdate<CR>", "Update" },
		},

		g = {
			name = "Git",
			s = { "<cmd>Neogit<CR>", "Status" },
		},

    f = {
      name = "Find",
      f = { "<cmd>lua require('utils.finder').find_files()<CR>", "Files" },
      b = { "<cmd>lua require('utils.finder').find_buffers()<CR>", "Buffers" },
      o = { "<cmd>FzfLua oldfiles<CR>", "Old Files" },
      g = { "<cmd>FzfLua live_grep<CR>", "Live Grep" },
      c = { "<cmd>FzfLua commands<CR>", "Commands" },
      e = { "<cmd>NvimTreeToggle<CR>", "Explorer" },
    },
	}

	whichkey.setup(conf)
	whichkey.register(mappings, opts)
end

return M




