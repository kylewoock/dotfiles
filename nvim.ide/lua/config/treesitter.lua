local M = {}

function M.setup()
	require("nvim-treesitter.configs").setup {
		-- Get all available parsers
		ensure_installed = "all",

		-- install languages synchrounously
		sync_install = false,

		highlight = {
			-- Setting this to false will disable the plugin
			enable = true,
		},
	}
end

return M
