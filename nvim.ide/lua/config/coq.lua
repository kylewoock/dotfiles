local M = {}

function M.setup()
  local coq = require "coq"
  coq.Now()

  require "coq_3p" {
    { src = "nvimlua", short_name = "nLua", conf_only = false },
    { src = "bc", short_name = "MATH", precision = 6 },
    {
      src = "repl",
      sh = "zsh",
      shell = { p = "pearl", n = "node"},
      max_lines = 99,
      deadline = 500,
      unsafe = { "rm", "poweroff", "mv" },
    },
  }
end

return M
