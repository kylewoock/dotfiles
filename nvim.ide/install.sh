#!/usr/bin/sh

NVIM_IDE=~/.config/nvim.ide/
export NVIM_IDE


rm -rf $NVIM_IDE
mkdir -p $NVIM_IDE/share
mkdir -p $NVIM_IDE/nvim

stow --restow --target=$NVIM_IDE/nvim .
alias nide='XDG_DATA_HOME=$NVIM_IDE/share
XDG_CONFIG_HOME=$NVIM_IDE nvim'
