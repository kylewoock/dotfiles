Development Environment Dotfiles
---

## Termainal

I use alacritty for my terminal emulator. This allows me to configure it through the YAML file and it is
the fastest terminal emulator

## Terminal Multiplexer

Since Alacritty doesn't have a lot of functionality built into it plus I like the ability to detach from specific sessions. Also
adds a better way for handling panes and windows

## Shell

The shell I mainly use is zsh. I have dabbled with fish as well but I usually end up going back to
zsh, as it has more plugins to use that I don't have rewrite the wheel.

I use the oh-my-zsh framework for base configuration and then do some light configuration for some of the tools
that I use.

## Starship

I use this for my terminal prompt this allows me to display the things that I would like displayed in my command prompt and it is very
customizable and allows you to do quite a bit.

## Editor

My primary editor for code is Neovim, you will need to at least have version 0.5.5-Nightly as I plugins that use the built in Neovim
lsp and I use treesitter pretty heavily

## Tree explorer

Broot is a tool that allows you to navigate filesystem tree fairly easily. It also allows you to take different actions on different 
files.

## Other Tools
- fzf: File system navigation
- bat: replacement for cat that has syntax highlighting
- z: File system navigation
- exa: replacement to ls that has coloring and icons
- ripgrep: faster replacement for grep
- fd: replacement for find
- kafkacat: reading kafka topics

