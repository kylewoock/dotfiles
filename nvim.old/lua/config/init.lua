return {
  keymenu = {
    which_key = true,
  },
  pde = {
    lua = true,
    docker = true,
    typescript = true,
    yaml = false,
    python = true,
    rust = false,
    go = true,
    csharp = false,
    json = true,
    jupyter = false,
    latex = true,
  },
}
