return {
  {
    "echasnovski/mini.jump",
    opts = {},
    keys = { "f", "F", "t", "T" },
    config = function(_, opts)
      require("mini.jump").setup(opts)
    end,
  },
}
