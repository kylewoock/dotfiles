-- TODO: Maybe this isn't necessary I never use any of the hydra stuff

-- UI
local function quick_menu()
  local cmd = require("hydra.keymap-util").cmd
  return {
    name = "quick menu",
    mode = { "n" },
    hint = [[
        quick menu
^
_f_: show terminal (float)      
_v_: open terminal (vertical)commands
_h_: open terminal (horizontal)
_x_: open quickfix
_l_: open location list
_s_: buffer fuzzy search        
_o_: open symbols outline      
_t_: show help tags            
_k_: show keymaps
_c_: show vim commands
_m_: show man pages            
^
^ ^  _q_/_<esc>_: exit hydra
    ]],
    config = {
      color = "teal",
      invoke_on_body = true,
      hint = {
        type = "window",
        position = "bottom",
        border = "rounded",
        show_name = true,
      },
    },
    body = "<a-q>",
    heads = {
      { "t", cmd "telescope help_tags", { desc = "open help tags", silent = true } },
      { "k", ":lua require('telescope.builtin').keymaps()<cr>", { desc = "open neovim keymaps", silent = true } },
      { "c", cmd "telescope commands", { desc = "open available telescope commands", silent = true } },
      { "m", cmd "telescope man_pages", { desc = "opens man pages", silent = true } },

      { "s", cmd "telescope current_buffer_fuzzy_find skip_empty_lines=true", { desc = "fuzzy find in current buffer", silent = true } },
      { "o", cmd "telescope aerial", { desc = "opens symbols outline", exit = true, silent = true } },

      { "x", cmd "troubletoggle quickfix", { desc = "opens quickfix", silent = true } },
      { "l", cmd "troubletoggle loclist", { desc = "opens location list", silent = true } },

      { "f", cmd "toggleterm direction=float", { desc = "floating terminal", silent = true } },
      { "v", cmd "toggleterm direction=vertical", { desc = "vertical terminal", silent = true } },
      { "h", cmd "toggleterm direction=horizontal", { desc = "horizontal terminal", silent = true } },

      { "q", nil, { desc = "quit", exit = true, nowait = true } },
      { "<esc>", nil, { desc = "quit", exit = true, nowait = true } },
    },
  }
end

local function gitsigns_menu()
  local gitsigns = require "gitsigns"

  local hint = [[
 _j_: next hunk   _s_: stage hunk        _d_: show deleted   _b_: blame line
 _k_: prev hunk   _u_: undo last stage   _p_: preview hunk   _b_: blame show full 
 ^ ^              _s_: stage buffer      ^ ^                 _/_: show base file
 ^
 ^ ^              _<enter>_: neogit              _q_: exit
]]

  return {
    name = "git",
    hint = hint,
    config = {
      color = "pink",
      invoke_on_body = true,
      hint = {
        border = "rounded",
        position = "bottom",
      },
      on_enter = function()
        vim.cmd "mkview"
        vim.cmd "silent! %foldopen!"
        vim.bo.modifiable = false
        gitsigns.toggle_signs(true)
        gitsigns.toggle_linehl(true)
      end,
      on_exit = function()
        local cursor_pos = vim.api.nvim_win_get_cursor(0)
        vim.cmd "loadview"
        vim.api.nvim_win_set_cursor(0, cursor_pos)
        vim.cmd "normal zv"
        gitsigns.toggle_signs(false)
        gitsigns.toggle_linehl(false)
        gitsigns.toggle_deleted(false)
      end,
    },
    body = "<a-g>",
    heads = {
      {
        "j",
        function()
          if vim.wo.diff then
            return "]c"
          end
          vim.schedule(function()
            gitsigns.next_hunk()
          end)
          return "<ignore>"
        end,
        { expr = true, desc = "next hunk" },
      },
      {
        "k",
        function()
          if vim.wo.diff then
            return "[c"
          end
          vim.schedule(function()
            gitsigns.prev_hunk()
          end)
          return "<ignore>"
        end,
        { expr = true, desc = "prev hunk" },
      },
      { "s", ":gitsigns stage_hunk<cr>", { silent = true, desc = "stage hunk" } },
      { "u", gitsigns.undo_stage_hunk, { desc = "undo last stage" } },
      { "s", gitsigns.stage_buffer, { desc = "stage buffer" } },
      { "p", gitsigns.preview_hunk, { desc = "preview hunk" } },
      { "d", gitsigns.toggle_deleted, { nowait = true, desc = "toggle deleted" } },
      { "b", gitsigns.blame_line, { desc = "blame" } },
      {
        "b",
        function()
          gitsigns.blame_line { full = true }
        end,
        { desc = "blame show full" },
      },
      { "/", gitsigns.show, { exit = true, desc = "show base file" } }, -- show the base of the file
      { "<enter>", "<cmd>neogit<cr>", { exit = true, desc = "neogit" } },
      { "q", nil, { exit = true, nowait = true, desc = "exit" } },
    },
  }
end

-- local function dap_menu()
--   local dap = require "dap"
--   local dapui = require "dapui"
--   local dap_widgets = require "dap.ui.widgets"

--   local hint = [[
--  _t_: toggle breakpoint             _r_: run to cursor
--  _s_: start                         _e_: evaluate input
--  _c_: continue                      _c_: conditional breakpoint
--  _b_: step back                     _u_: toggle ui
--  _d_: disconnect                    _s_: scopes
--  _e_: evaluate                      _x_: close
--  _g_: get session                   _i_: step into
--  _h_: hover variables               _o_: step over
--  _r_: toggle repl                   _u_: step out
--  _x_: terminate                     _p_: pause
--  ^ ^               _q_: quit
--   ]]

--   return {
--     name = "debug",
--     hint = hint,
--     config = {
--       color = "pink",
--       invoke_on_body = true,
--       hint = {
--         border = "rounded",
--         position = "middle-right",
--       },
--     },
--     mode = "n",
--     body = "<a-d>",
--     heads = {
--       {
--         "c",
--         function()
--           dap.set_breakpoint(vim.fn.input "[condition] > ")
--         end,
--         desc = "conditional breakpoint",
--       },
--       {
--         "e",
--         function()
--           dapui.eval(vim.fn.input "[expression] > ")
--         end,
--         desc = "evaluate input",
--       },
--       {
--         "r",
--         function()
--           dap.run_to_cursor()
--         end,
--         desc = "run to cursor",
--       },
--       {
--         "s",
--         function()
--           dap_widgets.scopes()
--         end,
--         desc = "scopes",
--       },
--       {
--         "u",
--         function()
--           dapui.toggle()
--         end,
--         desc = "toggle ui",
--       },
--       {
--         "x",
--         function()
--           dap.close()
--         end,
--         desc = "quit",
--       },
--       {
--         "b",
--         function()
--           dap.step_back()
--         end,
--         desc = "step back",
--       },
--       {
--         "c",
--         function()
--           dap.continue()
--         end,
--         desc = "continue",
--       },
--       {
--         "d",
--         function()
--           dap.disconnect()
--         end,
--         desc = "disconnect",
--       },
--       {
--         "e",
--         function()
--           dapui.eval()
--         end,
--         mode = { "n", "v" },
--         desc = "evaluate",
--       },
--       {
--         "g",
--         function()
--           dap.session()
--         end,
--         desc = "get session",
--       },
--       {
--         "h",
--         function()
--           dap_widgets.hover()
--         end,
--         desc = "hover variables",
--       },
--       {
--         "i",
--         function()
--           dap.step_into()
--         end,
--         desc = "step into",
--       },
--       {
--         "o",
--         function()
--           dap.step_over()
--         end,
--         desc = "step over",
--       },
--       {
--         "p",
--         function()
--           dap.pause.toggle()
--         end,
--         desc = "pause",
--       },
--       {
--         "r",
--         function()
--           dap.repl.toggle()
--         end,
--         desc = "toggle repl",
--       },
--       {
--         "s",
--         function()
--           dap.continue()
--         end,
--         desc = "start",
--       },
--       {
--         "t",
--         function()
--           dap.toggle_breakpoint()
--         end,
--         desc = "toggle breakpoint",
--       },
--       {
--         "u",
--         function()
--           dap.step_out()
--         end,
--         desc = "step out",
--       },
--       {
--         "x",
--         function()
--           dap.terminate()
--         end,
--         desc = "terminate",
--       },
--       { "q", nil, { exit = true, nowait = true, desc = "exit" } },
--     },
--   }
-- end

return {
  {
    "anuvyklack/hydra.nvim",
    event = { "bufreadpre" },
    opts = {
      specs = {
        gitsigns = gitsigns_menu,
        quick = quick_menu,
      },
    },
    config = function(_, opts)
      local hydra = require "hydra"
      for s, _ in pairs(opts.specs) do
        hydra(opts.specs[s]())
      end
    end,
  },
}
