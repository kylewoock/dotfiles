return {
  "vim-pandoc/vim-pandoc",
  event = "VeryLazy",
  dependencies = { "vim-pandoc/vim-pandoc-syntax" },
}
