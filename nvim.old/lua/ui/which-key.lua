return {
  {
    "mrjones2014/legendary.nvim",
    keys = {
      { "<C-S-p>", "<cmd>Legendary<CR>", desc = "Legendary" },
      { "<leader>hc", "<cmd>Legendary<CR>", desc = "Command Palette" },
    },
    opts = {
      which_key = { auto_register = true },
    },
  },
  {
    "folke/which-key.nvim",
    dependencies = {
      "mrjones2014/legendary.nvim",
    },
    event = "VeryLazy",
    config = function()
      local wk = require "which-key"
      wk.setup {
        show_help = false,
        plugins = { spelling = true },
        key_labels = { ["<leader>"] = "SPC" },
        triggers = "auto",
      }
      wk.register({ prefix = "<leader>" }, {
        w = { "<cmd>update!<CR>", "Save" },
        q = { "<cmd>lua require('util').smart_quit()<CR>", "Quit" },
        f = { name = "+File" },
        g = { name = "+Git" },
        d = { name = "DAP" },
        D = { name = "+Database" },
        c = {
          name = "+Code",
          x = {
            name = "Swap Next",
            f = "Function",
            p = "Pararmeter",
            c = "Class",
          },
          X = {
            name = "Swap Previous",
            f = "Function",
            p = "Parameter",
            c = "Class",
          },
        },
        s = {
          name = "+Search",
          c = {
            function()
              require("utils.cht").cht()
            end,
            "Cheatsheets",
          },
          s = {
            function()
              require("utils.cht").stack_overflow()
            end,
            "Stack Overflow",
          },
        },
      })
    end,
  },
}
