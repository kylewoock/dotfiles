return {
  "andymass/vim-mathcup",
  lazy = false,
  enabled = false,
  config = function()
    vim.g.matchup_matchparen_offscreen = { method = "popup" }
  end,
}
