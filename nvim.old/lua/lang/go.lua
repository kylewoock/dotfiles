if not require("config").pde.go then
  return {}
end

return {
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, { "go", "gomod", "gowork", "gosum", "gotmpl" })
    end,
  },
  {
    "williamboman/mason.nvim",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "delve",
        "gotests",
        "golangci-lint",
        "gofumpt",
        "goimports",
        "golangci-lint-langserver",
        "impl",
        "gomodifytags",
        "iferr",
        "gotestsum",
      })
    end,
  },
  {
    "nvimtools/none-ls.nvim",
    opts = function(_, opts)
      local nls = require "null-ls"
      if type(opts.sources) == table then
        vim.list_extend(opts.sources, {
          nls.builtins.code_actions.gomodifytags,
          nls.builtins.code_actions.impl,
          nls.builtins.formatting.gofumpt,
          nls.builtins.formatting.goimports_reviser,
        })
      end
    end,
  },
  {
    "ray-x/go.nvim",
    dependencies = {
      "ray-x/guihua.lua",
      "neovim/nvim-lspconfig",
      "nvim-treesitter/nvim-treesitter",
    },
    opts = {},
    config = function(_, opts)
      require("go").setup(opts)
    end,
    event = { "CmdlineEnter" },
    ft = { "go", "gomod" },
    build = ':lua require("go.install").update_all_sync()',
  },
  {
    "neovim/nvim-lspconfig",
    opts = {
      servers = {
        gopls = {
          settings = {
            analyses = {
              unusedparams = true,
            },
            hints = {
              assignVariableTypes = true,
              compositeLiteralFields = true,
              compositeLiteralTypes = true,
              constantValues = true,
              functionTypeParameters = true,
              parameterNames = true,
              rangeVariableTypes = true,
            },
            staticcheck = true,
            semanticTokens = true,
          },
        },
      },
      golangci_list_ls = {},
    },
    setup = {
      gopls = function(_, _)
        local lsp_utils = require "editor.lsp.utils"
        lsp_utils.on_attach(function(client, bufnr)
          local map = function(mode, lhs, rhs, desc)
            if desc then
              desc = desc
            end
            vim.keympa.set(mode, lhs, rhs, { silent = true, desc = desc, buffer = bufnr, noremap = true })
          end
          if client.name == "gopls" then
            map("n", "<leader>ly", "<cmd>GoModTidy<cr>", "Go Mod Tidy")
            map("n", "<leader>lc", "<cmd>GoCoverage<Cr>", "Go Test Coverage")
            map("n", "<leader>lt", "<cmd>GoTest<Cr>", "Go Test")
            map("n", "<leader>lR", "<cmd>GoRun<Cr>", "Go Run")
            map("n", "<leader>dT", "<cmd>lua require('dap-go').debug_test()<cr>", "Go Debug Test")

            if not client.server_capabilities.semanticTokensProvider then
              local semantic = client.config.capabilities.textDocument.semanticTokens
              client.server_capabilities.semanticTokensProvider = {
                full = true,
                legend = {
                  tokenTypes = semantic.TokenTypes,
                  tokenModifiers = semantic.tokenModifiers,
                },
                range = true,
              }
            end
          end
        end)
      end,
    },
  },
}
