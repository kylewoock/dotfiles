if not require("config").pde.docker then
  return {}
end

return {
  'lervag/vimtex',
  lazy = false,
  init = function ()
    vim.g.vimtext_view_method = "zathura"
  end
}
